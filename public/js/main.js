$(document).ready(function(argument) {
    // =====================================
    // -------------------------------------
    // =====================================
    function banner() {
        $('.banner .owl-carousel').owlCarousel({
            loop: true,
            margin: 0,
            nav: false,
            dots: true,
            autoplay: true,
            autoplayTimeout: 8000,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1,
                    autoHeight: true
                },
                600: {
                    items: 1,
                    autoHeight: true
                },
                1200: {
                    items: 1
                }
            }
        })
    }
    banner();
    // =====================================
    function useful() {
        $('.owl-useful .owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            navText: [`<img src="img/uleft.png">`, `<img src="img/uright.png">`],
            dots: false,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1200: {
                    items: 3
                }
            }
        })
    }
    useful();
    // =====================================
    function menu() {
        $('.menu .navbar-nav .nav-item').mouseenter(function(argument) {

            if ($(window).width() >= 1200) {
                $(this).addClass('show');
                $(this).children('.dropdown-menu').addClass('show');
            }
        })
        $('.menu .navbar-nav .nav-item').mouseleave(function(argument) {
    
            if ($(window).width() >= 1200) {
                $(this).removeClass('show');
                $(this).children('.dropdown-menu').removeClass('show');
            }
    
        })
    }
    menu();
    // =====================================
    // =====================================
    // =====================================
    function mobile(params) {
        $('.mobile button').click(function () {
            window.open(window.location.href,"_blank","toolbar=yes, location=yes, directories=no, status=no, menubar=yes, scrollbars=yes, resizable=no, copyhistory=yes, width=450, height=600");
        })    
    }
    mobile();
    // =====================================
    
    function mycopy(params) {
        $('#urlbtn').click(function () {
            var copyText = document.getElementById("inputurl");
            copyText.select();
            copyText.setSelectionRange(0, 99999);
            document.execCommand("copy");
            var tooltip = $("#copyurl");
            var title = tooltip.data('title');
            tooltip.html(title);
        })
    }
    mycopy();
    // =====================================
    setInterval(function () {
        $('.loader-content').addClass('d-none')
    }, 1500)
    // =====================================
    // =====================================
    setInterval(function() {
        banner();
        useful();
        menu();
        mobile();
        mycopy();
    }, 1000)
    // =====================================
})